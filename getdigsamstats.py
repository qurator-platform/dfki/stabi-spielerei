import os
import sys
import codecs
import json
from collections import defaultdict

rootfolder = os.path.abspath('digsamcollection')
manifests = {}
for subfolder in os.listdir(rootfolder):
    for f in os.listdir(os.path.join(rootfolder, subfolder)):
        if f.endswith('manifest.json'):
            abspath = os.path.abspath(os.path.join(rootfolder, subfolder, f))
            try:
                js = json.load(codecs.open(abspath))
                manifests[os.path.basename(abspath).split('.')[0]] = js
            except json.decoder.JSONDecodeError:
                sys.stderr.write('WARNING: Failed to load %s\n' % abspath)

languages = defaultdict(int)
categories = defaultdict(int)
earliest_erscheinungsjahr = 2019
latest_erscheinungsjahr = 0

for man in manifests:
    metadata = manifests[man]['metadata']
    for val in metadata:
        if val['label'] == 'Sprache':
            if isinstance(val['value'], list):
                for subval in val['value']:
                    for ssv in subval:
                        languages[subval[ssv]] += 1
            elif isinstance(val['value'], str):
                languages[val['value']] += 1
        if val['label'] == 'Kategorie':
            if isinstance(val['value'], list):
                for subval in val['value']:
                    for ssv in subval:
                        categories[subval[ssv]] += 1
            elif isinstance(val['value'], str):
                categories[val['value']] += 1
        if val['label'] == 'Erscheinungsjahr':
            try:
                intval = int(val['value'])
                if intval < earliest_erscheinungsjahr:
                    earliest_erscheinungsjahr = intval
                    if intval > latest_erscheinungsjahr:
                        latest_erscheinungsjahr = intval
            except TypeError:
                pass
print('earliest erscheinungsjahr:', earliest_erscheinungsjahr)
print('latest erscheinungsjahr:', latest_erscheinungsjahr)

for pair in sorted(languages.items(), key = lambda x: x[1], reverse=True):
    print('%s\t%s' % (pair[0], pair[1]))

for pair in sorted(categories.items(), key = lambda x: x[1], reverse=True):
    print('%s\t%s' % (pair[0], pair[1]))

        

