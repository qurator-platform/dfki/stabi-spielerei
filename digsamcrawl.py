import requests
import zipfile
import io
import re
import codecs
from optparse import OptionParser
import shutil
import sys
import os
import json
import time
from progressbar import *

#rooturl = 'CONFIGURE_API_ENDPOINT_HERE'
rooturl = 'https://content.staatsbibliothek-berlin.de/dc'

# some docs here: https://ngcs-beta.staatsbibliothek-berlin.de/docs#

def download_txt(ppns, outrootfolder):

    widgets = ['Collecting fulltext and manifest for PPNs: ', Percentage(), ' ', Bar(marker='-',left='[',right=']'), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=len(ppns))
    pbar.start()

    sleep_interval = 10 # seconds
    fi = 0
    for i, ppn in enumerate(ppns):
        pbar.update(i)
        if i % 100 == 0:
            fi += 1
            time.sleep(sleep_interval)
        try:
            outf = os.path.join(outrootfolder, str(fi))
            if not os.path.exists(outf):
                os.makedirs(outf)
            if os.path.exists(os.path.join(outf, '%s.manifest.json' % ppn)):
                pass
            else:
                manifest_url = '%s/PPN%s/manifest' % (rooturl, ppn)
                man = json.loads(requests.get(manifest_url).text)
                json.dump(man, codecs.open(os.path.join(outf, '%s.manifest.json' % ppn), 'w'), indent=2)
            if os.path.exists(os.path.join(outf, '%s.plaintext.txt' % ppn)):
                #if os.path.exists(os.path.join(outf, '%s.mets.xml' % ppn)):
                pass
            else:
                fulltext_url = '%s/PPN%s.ocr.txt' % (rooturl, ppn)
                #fulltext_url = '%s/PPN%s.mets.xml' % (rooturl, ppn)
                text = requests.get(fulltext_url).text
                txtout = codecs.open(os.path.join(outf, '%s.plaintext.txt' % ppn), 'w')
                #txtout = codecs.open(os.path.join(outf, '%s.mets.xml' % ppn), 'w')
                txtout.write(text)
                txtout.close()
        except:
            sys.stderr.write('WARNING: Failed to retrieve info for %s.\n' % ppn)
    pbar.finish()

def getLangsFromManifest(manifest):
    
    metadata = manifest['metadata']
    languages = set()
    for val in metadata:
        if val['label'] == 'Sprache':
            if isinstance(val['value'], list):
                for subval in val['value']:
                    for ssv in subval:
                        languages.add(subval[ssv])
            elif isinstance(val['value'], str):
                languages.add(val['value'])
    return languages

def getNrPagesFromManifest(manifest):

    sequences = manifest['sequences']
    canvases = sequences[0]['canvases']
    nr_pages = len(canvases)
    return nr_pages
    
def download_images(ppns, lang, outrootfolder):

    top_n = 20 # first n pages to be downloaded
    
    if not os.path.exists(os.path.join(outrootfolder, lang)):
        os.makedirs(os.path.join(outrootfolder, lang))

    widgets = ['Collecting images for PPNs: ', Percentage(), ' ', Bar(marker='-',left='[',right=']'), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=len(ppns))
    pbar.start()

    for i, ppn in enumerate(ppns):
        pbar.update(i)
        try:
            # not sure if this always works (let's see), but first get the manifest to get language and number of pages, then download tiff file page by page for the specified lang
            manifest_url = '%s/PPN%s/manifest' % (rooturl, ppn)
            man = json.loads(requests.get(manifest_url).text)
            flangs = getLangsFromManifest(man)
            nr_pages = getNrPagesFromManifest(man)
            if lang in flangs:
                # make folder for every ppn
                ppnfolder = os.path.join(os.path.join(outrootfolder, lang), ppn)
                if not os.path.exists(ppnfolder):
                    os.makedirs(ppnfolder)

                for pageno in range(1, min(top_n+1, nr_pages+1)):
                    tif_url = '%s/PPN%s-%s/full/full/0/default.tif' % (rooturl, ppn, str('%04d' % pageno))
                    tif = requests.get(tif_url, stream=True)
                    outtifname = '%s-%s.tif' % (ppn, str('%04d' % pageno))
                    outtifabsloc = os.path.join(ppnfolder, outtifname)
                    shutil.copyfileobj(tif.raw, codecs.open(outtifabsloc, 'wb'))
        except:
            sys.stderr.write('WARNING: Failed to retrieve info for %s.\n' % ppn)

    pbar.finish()
    
        
    
if __name__ == '__main__':

    parser = OptionParser('Usage: %prog -options')
    parser.add_option('-p', '--ppns', dest='ppns', help='raw text file containing ppn ids (one per line)')
    
    options, args = parser.parse_args()

    if not options.ppns:
        parser.print_help(sys.stderr)
        sys.exit(1)

    outfbasename = 'digsamcollection'
    if not os.path.exists(options.ppns):
        sys.stderr.write('ERROR: Could not find file %s.\n' % options.ppns)
    fp = os.path.dirname(os.path.abspath(__file__))
    if not os.path.exists(os.path.join(fp, outfbasename)):
        os.makedirs(os.path.join(fp, outfbasename))
    outrootfolder = os.path.join(fp, outfbasename)
    if rooturl == 'CONFIGURE_API_ENDPOINT_HERE':
        sys.stderr.write('ERROR: Please configure the root url for the stabi api...\n')
        sys.exit(1)
        
    ppns = [x.strip() for x in codecs.open(options.ppns).readlines()]

    # use this one to collect ocr plaintext output:
    #download_txt(ppns, outrootfolder) 

    outimagebasename = 'tif_images'
    if not os.path.exists(os.path.join(fp, outimagebasename)):
        os.makedirs(os.path.join(fp, outimagebasename))
    outimagerootfolder = os.path.join(fp, outimagebasename)

    #debugppns = ['607950927']
    # use this one to collect images:
    download_images(ppns, 'dut', outimagerootfolder) # specify lang of interest
    
    
    
